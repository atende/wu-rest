package info.atende.webutil.rest;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Month;
import java.util.Date;

@RunWith(Arquillian.class)

public class JacksonContextResolverTestDefault {
    @Deployment
    public static Archive<?> createTestArchive(){
        Archive<?> archive = ShrinkWrap.create(JavaArchive.class)
                .addClass(JacksonContextResolver.class)
                .addClass(JacksonMapperConfig.class)
                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
        return archive;
    }

    @Inject
    JacksonContextResolver resolver;

    @Test
    public void shouldFormatDateWithDefaultDateFormat(){
        SimpleDateFormat dateFormat = new SimpleDateFormat(JacksonContextResolver.DEFAULT_DATE_FORMAT);
        Date date = new Date();
        String dateString = dateFormat.format(date);
        String date2 = resolver.getContext(ExtResponse.class).getSerializationConfig().getDateFormat().format(date);
        Assert.assertEquals(dateString, date2);
    }
    @Test
    public void shouldNotSerializeNullByDefault(){
        Assert.assertEquals(JsonInclude.Include.NON_NULL, resolver.getContext(ExtResponse.class).getSerializationConfig().getSerializationInclusion());
    }
    @Test
    public void shouldConvertJava8DateApi(){
        LocalDate localDate = LocalDate.of(2014, Month.DECEMBER,24);
        Instant instant = Instant.EPOCH;
        LocalTime localTime = LocalTime.of(9,0);
        try {
            ObjectMapper mapper = resolver.getContext(ExtResponse.class);
            String dateString = mapper.writeValueAsString(localDate);
            Assert.assertEquals("\"2014-12-24\"", dateString);
            String instantString = mapper.writeValueAsString(instant);
            Assert.assertEquals("\"1970-01-01T00:00:00Z\"", instantString);
            String locaTimeString = mapper.writeValueAsString(localTime);
            Assert.assertEquals("\"09:00\"", locaTimeString);
        } catch (JsonProcessingException e) {
           Assert.fail("Can't serialize date " + e.getMessage());
        }
    }
    /* The JODA converter was remove in favor of JDK 8
    @Test
    public void shouldConverJodaTimeDateApi(){
        org.joda.time.LocalDate localDate = new org.joda.time.LocalDate(2014, 12,24);
        org.joda.time.Instant instant = new org.joda.time.Instant(Instant.EPOCH.toEpochMilli());
        org.joda.time.LocalTime localTime = new org.joda.time.LocalTime(9,0);
        try {
            ObjectMapper mapper = resolver.getContext(ExtResponse.class);
            String dateString = mapper.writeValueAsString(localDate);
            Assert.assertEquals("\"2014-12-24\"", dateString);
            String instantString = mapper.writeValueAsString(instant);
            Assert.assertEquals("\"1970-01-01T00:00:00.000Z\"", instantString);
            String locaTimeString = mapper.writeValueAsString(localTime);
            Assert.assertEquals("\"09:00:00.000\"", locaTimeString);
        } catch (JsonProcessingException e) {
            Assert.fail("Can't serialize date " + e.getMessage());
        }

    }*/
}