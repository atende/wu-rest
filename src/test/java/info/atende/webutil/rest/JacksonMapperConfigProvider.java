package info.atende.webutil.rest;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import javax.enterprise.inject.Produces;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * Provider for Test
 * @author Giovanni Silva
 * Date: 8/15/14.
 */
public class JacksonMapperConfigProvider {
    public static final String format = "dd-MM-yy";
    @Produces
    public JacksonMapperConfig provideDateMask(){
        return new JacksonMapperConfig(){
            @Override
            public void configMapper(ObjectMapper mapper) {
                mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
                DateFormat dateFormat = new SimpleDateFormat(format);
                mapper.setDateFormat(dateFormat);
                mapper.setSerializationInclusion(JsonInclude.Include.ALWAYS);
            }
        };
    }
}
