package info.atende.webutil.rest;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Giovanni Silva
 * Date: 8/15/14.
 */
@RunWith(Arquillian.class)
public class JacksonContextResolverTestProvider {
    @Deployment
    public static Archive<?> createsArquive(){
        Archive<?> archive = ShrinkWrap.create(JavaArchive.class)
                .addClass(JacksonMapperConfigProvider.class)
                .addPackage(JacksonContextResolver.class.getPackage())
                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
        return archive;
    }
    @Inject
    JacksonContextResolver resolver;
    @Inject
    JacksonMapperConfig configProvided;

    @Test
    public void providerMask(){
        SimpleDateFormat dateFormat = new SimpleDateFormat(JacksonMapperConfigProvider.format);
        Date date = new Date();
        String dateString = dateFormat.format(date);
        String date2 = resolver.getContext(ExtResponse.class).getSerializationConfig().getDateFormat().format(date);
        Assert.assertEquals(dateString, date2);
    }
    @Test
    public void shouldSerializeNullAsOverridedByProvided(){
        Assert.assertEquals(JsonInclude.Include.ALWAYS, resolver.getContext(ExtResponse.class).getSerializationConfig().getSerializationInclusion());
    }

}
