package info.atende.webutil.rest;

import java.util.Collection;

/**
 * Base class which represents a minimal payload to be returned
 * to an External AJAX request, containing a optional message and data. <br>
 * These fields represent 90% of cases when doing with responses, when you make this a pattern you could process in the
 * frontend this information. By example if there is a message display it to the user.
 * @author Giovanni Silva
 * Date: 6/4/13
 * Time: 10:31 PM
 */
public class ExtResponse<T> {
    /**
     * The intention of message is feedback
     */
    protected  String message;
    protected Collection<T> data;

    public ExtResponse(String message, Collection<T> data){
        this.message = message;
        this.data = data;
    }
    public ExtResponse(String message) {
        this.message = message;
    }

    public ExtResponse(Collection<T> data){
        this.data = data;
    }

    public ExtResponse() {

    }

    public String getMessage() {
        return message;
    }


    public void setMessage(String message) {
        this.message = message;
    }


    public Collection<T> getData() {
        return data;
    }

    public void setData(Collection<T> data) {
        this.data = data;
    }
}
