package info.atende.webutil.rest;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.hibernate4.Hibernate4Module;
import com.fasterxml.jackson.datatype.jsr310.JSR310Module;

import javax.annotation.PostConstruct;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.logging.Logger;

/**
 * Customize the provider for JSON Serialization.
 * It register Hibernate 4 Module to prevents Lazy Instantiation Exception, register the JodaModule and
 * format the Date.<br>
 * To override or custom the objectMapper, simple create a provider for {@link JacksonMapperConfig}
 * @author Giovanni Silva
 * Date: 6/4/13
 * Time: 11:48 PM
 */

@Provider
@Produces(MediaType.APPLICATION_JSON)
public class JacksonContextResolver implements ContextResolver<ObjectMapper> {
    private final static Logger logger = Logger.getLogger(JacksonContextResolver.class.getName());
    private ObjectMapper objectMapper;
    /**
     * User injected mapper config
     */
    @Inject
    private Instance<JacksonMapperConfig> mapperOverrides;
    // The default date format used in DefaultMapperConfig
    public final static String DEFAULT_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
    @PostConstruct
    public void init() {
        this.objectMapper = new ObjectMapper();
        /**
         * Prevents Hibernate Lazy Instantiation
         */
        Hibernate4Module hm = new Hibernate4Module();
        this.objectMapper.registerModule(hm);
        // Correct Serialization of JDK 8 javax.time API's
        JSR310Module jsr310Module = new JSR310Module();
        this.objectMapper.registerModule(jsr310Module);
        /**
         * Configures de Mapper with DefaultMapperConfig
         */
        configMapper(objectMapper);
        /**
         * Let the user provider overrides the defaultConfigs
         */
        JacksonMapperConfig mapperConfig = null;
        try {
            mapperConfig = mapperOverrides.get();
            mapperConfig.configMapper(objectMapper);
        }catch (Exception e){ // If a exception is throw, could not inject the bean. Use default Config
            logger.info("There is no configuration for JacksonMapperConfig. To customize Json Serialization " +
                    "add a provider for that");
        }

    }
    @Override
    public ObjectMapper getContext(Class<?> aClass) {
        return objectMapper;
    }
    private void configMapper(ObjectMapper mapper) {
        mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        DateFormat dateFormat = new SimpleDateFormat(DEFAULT_DATE_FORMAT);
        mapper.setDateFormat(dateFormat);
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
    }
}

