package info.atende.webutil.rest;


import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * This class represents a point to override {@link com.fasterxml.jackson.databind.ObjectMapper} configurations. <br >
 * Is used by {@link info.atende.webutil.rest.JacksonContextResolver} to personalize the serialization
 * @author Giovanni Silva
 * Date: 8/15/14.
 */
public interface JacksonMapperConfig {
    void configMapper(ObjectMapper mapper);
}
