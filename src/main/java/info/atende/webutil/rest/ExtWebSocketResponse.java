package info.atende.webutil.rest;

import java.util.Collection;

/**
 * A sub-class of ExtResponse with more fields to websocket communication
 * @author Giovanni Silva
 */
public class ExtWebSocketResponse<T> extends ExtResponse<T> {
    /**
     * Topic is a generic way to categorize this payload in some group.
     */
    protected  String topic;
    /**
     * If this message is important to the user
     */
    protected  Boolean important;

    public ExtWebSocketResponse(String message, Collection<T> data){
        super(message, data);

    }
    public ExtWebSocketResponse(String message) {
        super(message);

    }

    public ExtWebSocketResponse(Collection<T> data){
        super(data);

    }

    public ExtWebSocketResponse() {

    }
    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public Boolean getImportant() {
        return important;
    }

    public void setImportant(Boolean important) {
        this.important = important;
    }
}
